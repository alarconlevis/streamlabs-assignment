<?php
 
require_once 'database_config.php';
 
$dsn= "mysql:host=$host;dbname=$db";
 
try{
 // create a PDO connection with the configuration data
 $conn = new PDO($dsn, $username, $password);
 
 // display a message if connected to database successfully
 	if($conn){
					
				if($_SERVER['REQUEST_METHOD'] === 'POST'){
					if($_POST['messages']){
						$messages = json_decode($_POST["messages"]);
						//echo '<pre>' . print_r($messages, true); exit;	    	
	 		    		foreach ($messages as $m) {
	 		    			$statement = $conn->prepare("INSERT INTO messages(message, user)
						    VALUES(:message, :user)");
							$statement->execute(array(
							    "message" => $m->message,
							    "user" => $m->username
							));
	 		    		}
 		    		}
				}elseif ($_SERVER['REQUEST_METHOD'] === 'GET'){
					if($_GET['user']){
						$statement = $conn->prepare('SELECT user, message FROM messages
						    WHERE user = :user');
						$statement->bindParam(':user', $_GET['user']);
						$statement->execute();
						$messages = $statement->fetchAll(PDO::FETCH_ASSOC);
	 		    		header('Content-type: application/json');
						echo json_encode($messages);
					}
 		    	}
					$conn = null;
    }
}catch (PDOException $e){
 // report error message
 echo $e->getMessage();
}