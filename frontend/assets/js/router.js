var login = $('#login');
var list = $('#list');
var streaming = $('#stream');
var messageFilter = $('#messageFilter');

var messages = [];
var forPhpArray = [];


function redirect(fromPage, toPage) {
	fromPage.hide();
	toPage.show();
}

function joinStreaming(chanId, vidId) {
	let forPhpArray = [];
	let src = 'https://www.youtube.com/embed/live_stream?channel=' + chanId;
	$('#streaming').attr('src', src);
	redirect(list, streaming);
	gapi.client.youtube.videos.list({
			part: "id, snippet, liveStreamingDetails",
			id: vidId,
			maxResults: "1"
		})
		.then(response => {
				setInterval(function(){
					getMessages(response.result.items[0].liveStreamingDetails.activeLiveChatId)
				}, 2000);
			},
			function(err) {
				console.error("Execute error", err);
			});
}

function showMessageFilter(){
	redirect(streaming, messageFilter)
}


