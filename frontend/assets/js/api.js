        // Client ID and API key from the Developer Console
        var CLIENT_ID = '588124519299-c3srg1q54gma5m378c5d2tvlua308a3f.apps.googleusercontent.com';

        // Array of API discovery doc URLs for APIs used by the quickstart
        var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest"];

        // Authorization scopes required by the API. If using multiple scopes,
        // separated them with spaces.
        var SCOPES = 'https://www.googleapis.com/auth/youtube.readonly';

        var authorizeButton = document.getElementById('authorize-button');
        var signoutButton = document.getElementById('signout-button');
        var newMessagesNumber = 0;


        /**
         *  On load, called to load the auth2 library and API client library.
         */
        function handleClientLoad() {
          gapi.load('client:auth2', initClient);
        }

        /**
         *  Initializes the API client library and sets up sign-in state
         *  listeners.
         */
        function initClient() {
          gapi.client.init({
            discoveryDocs: DISCOVERY_DOCS,
            clientId: CLIENT_ID,
            scope: SCOPES
          }).then(function() {
            // Listen for sign-in state changes.
            gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

            // Handle the initial sign-in state.
            updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
            authorizeButton.onclick = handleAuthClick;
            signoutButton.onclick = handleSignoutClick;
          });
        }

        /**
         *  Called when the signed in status changes, to update the UI
         *  appropriately. After a sign-in, the API is called.
         */
        function updateSigninStatus(isSignedIn) {
          if (isSignedIn) {
            authorizeButton.style.display = 'none';
            signoutButton.style.display = 'block';
            redirect($('#login'), $('#list'));
            getChannels();
          } else {
            authorizeButton.style.display = 'block';
            signoutButton.style.display = 'none';
          }
        }

        /**
         *  Sign in the user upon button click.
         */
        function handleAuthClick(event) {
          gapi.auth2.getAuthInstance().signIn();
        }

        /**
         *  Sign out the user upon button click.
         */
        function handleSignoutClick(event) {
          gapi.auth2.getAuthInstance().signOut();
          document.location.href="index.html";

        }

        /**
         * Append text to a pre element in the body, adding the given message
         * to a text node in that element. Used to display info from API response.
         *
         * @param {string} message Text to be placed in pre element.
         */
        function appendPre(message) {
          var pre = document.getElementById('content');
          var textContent = document.createTextNode(message + '\n');
          pre.appendChild(textContent);
        }

        /**
         * Print files.
         */

        function getChannels() {
          gapi.client.youtube.search.list({
            part: 'snippet, id',
            maxResults: 5,
            type: 'video',
            eventType: 'live',
            videoCategoryId: '10',
            regionCode: 'US',
          }).then(function(resp) {
            console.log(resp);
            let resultItems = resp.result.items;
            $.each(resultItems, function(index, item) {
              vidTitle = item.snippet.title;
              chanId = item.snippet.channelId;
              vidId = item.id.videoId;
              vidThumburl = item.snippet.thumbnails.default.url;
              vidThumbimg = `<pre><img id="thumb" src="${vidThumburl} " alt="No  Image Available." style="width:204px;height:128px"></pre>`;
              $('#results').append(`<li class="list-group-item video-item"> <pre onclick="joinStreaming('${chanId}', '${vidId}')"> ${vidTitle} ${vidThumbimg}</pre> </li>`);
            });

          });

        }

        function getMessages(activeLiveChatId) {
          gapi.client.youtube.liveChatMessages.list({
              "liveChatId": activeLiveChatId,
              "part": "id, snippet, authorDetails"
            }).then(function(response) {
              return JSON.parse(response.body);
            })
            .then(function(response) {
              console.log(response);
              let authorName;
              let authorImage;
              let message;
              let alreadySent;
              // Handle the results here (response.result has the parsed body).
              response.items.forEach(function(items) {
                authorName = items.authorDetails.displayName;
                authorImage = items.authorDetails.profileImageUrl;
                message = items.snippet.displayMessage;

                $('#messages').append(`<p> <b>${authorName} </b> ${message}</p>`)

                alreadySent = messages.find(function(user) {
                  return user.username === authorName && user.message === message;
                });

                if (!alreadySent) {
                  messages.push({
                    username: authorName,
                    message: message
                  });

                  forPhpArray.push({
                    username: authorName,
                    message: message
                  });
                  newMessagesNumber++;
                  console.log(newMessagesNumber);
                  $('#newMessagesNumber').text(newMessagesNumber);
                }
              });

              if (forPhpArray.length) {
                $.post("../backend/queries.php", {
                    messages: JSON.stringify(forPhpArray)
                  }, function() {
                    console.log("success");
                  })
                  .always(function() {
                    forPhpArray.splice(0, forPhpArray.lenght);
                    forPhpArray = [];
                  });
              }
            });
        }

        function getMessagesByUser(){
            let username = $('#username').val();
            $.get("../backend/queries.php?user=" + username, function(messages) {
              console.log(messages);
              $('#messageBox').empty();
              messages.forEach( m => {
                $('#messageBox').append(`<p> <b>${m.user} </b> ${m.message}</p>`);
              })
            });
        }

